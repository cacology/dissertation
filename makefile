pdftargets := $(patsubst %.md,%.pdf,$(wildcard *.md))
mdxtargets := $(patsubst %.md,%.mdx,$(wildcard *.md))

listpdftargets : ; @echo $(pdftargets)
listmdxtargets : ; @echo $(mdxtargets)

all: assemblage.pdf
.PHONY : all

allmdx : $(mdxtargets)
.PHONY : allmdx

%.mdx : %.md
	pandoc -t markdown-citations -f markdown-latex_macros --citeproc -o $@ $<

assemblage.tex : $(mdxtargets)
	pandoc -s --file-scope -f markdown-latex_macros+fenced_code_blocks+fenced_code_attributes -t context -o assemblage.tex headers.yaml $(mdxtargets)

assemblage.docx : $(mdxtargets)
	pandoc -s --file-scope -f markdown-latex_macros+fenced_code_blocks+fenced_code_attributes -t docx -o assemblage.docx headers.yaml $(mdxtargets)

%.tex : %.md
	pandoc -s -f markdown+fenced_code_blocks+fenced_code_attributes -t context --citeproc -o $@ headers.yaml $<

%.pdf : %.tex
	context $< > log.txt

%.docx : %.md
	pandoc -s -f markdown+fenced_code_blocks+fenced_code_attributes -t docx --citeproc -o $@ headers.yaml $<

%.odt : %.md
	pandoc -s -f markdown+fenced_code_blocks+fenced_code_attributes --citeproc -o $@ $<

.PHONY : clean
clean :
	find . \( -name "*.tuc" -o -name "*.log" -o -name "*.tex" -o -name "*.mdx" -o -name "*-export" -o -name "*.txt" \) \! -iregex ".*rev.*" -delete
	find . \( -name "*-export" \) -exec rm -rf {} +

.PHONY : cleanall
cleanall :
	find . \( -name "*.pdf" -o -name "*.tuc" -o -name "*.log" -o -name "*.tex" -o -name "*.mdx" -o -name "*.docx" -o -name "*.odt" -o -name "*.txt" \) \! -iregex ".*rev.*" -delete
	find . \( -name "*-export" \) -exec rm -rf {} +
